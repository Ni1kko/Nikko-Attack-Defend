/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/
 
params [ 
	["_roundInterval", 45, [0]],
	["_maxRoundTime", 15, [0]]
];

//Get world Config
if !(([] call (missionNamespace getVariable [format["NikkoServer_script_FightZoneCfg_%1",worldName], {[]}])) params [
	["_MinDistanceFromSpawnZone",0,[0]], 
	["_MinSurfaceNormal",0,[0]], 
	["_MinDistFromBorders",[],[[]]], 
	["_AllMissions",[],[[]]]
])exitWith{diag_log "BAD WORLD FIGHTZONE CONFIG";};

//Get random mission
FightZoneMission = (selectRandom _AllMissions);
 
//Setup Mission & Get Pos off new mission
[
	_MinDistanceFromSpawnZone,
	_MinSurfaceNormal
] call (missionNamespace getVariable [format["NikkoServer_script_FightZone_%1",FightZoneMission], {[]}]) params [
	["_selectedMissionLocation",[]],
	["_selectedMissionRadius",350]
];

if(count _selectedMissionLocation < 1)exitWith{
	diag_log "BAD FIGHTZONE LOCATION";
	[]
};

//Markers to create
Nikko_Var_FightZoneMarkers pushBackUnique "FightZoneMarkerCircle";
private _circle = createMarker ["FightZoneMarkerCircle", _selectedMissionLocation];
_circle setMarkerColor "ColorRed";
_circle setMarkerShape "ELLIPSE";
_circle setMarkerBrush "Solid";
_circle setMarkerSize [_selectedMissionRadius,_selectedMissionRadius];
_circle setMarkerDir 20;
_circle setMarkerText "FightZone";


publicVariable "Nikko_Var_FightZoneMarkers"; 

//update round
missionNamespace setVariable ["Nikko_Var_roundTime", (((_maxRoundTime * 60) + _roundInterval) + serverTime), true];
missionNamespace setVariable ["Nikko_Var_RoundInProgress", true, true];

format ["(NewRound) Round #%1 | Interval: %2 Secs | Length: %3 Mins | Pos: "+str(_selectedMissionLocation), Nikko_Var_RoundsCount,_roundInterval,_maxRoundTime] call doErrorMsg;

missionNamespace setVariable ["Nikko_Var_selectedMissionLocation", _selectedMissionLocation, true];
[(_roundInterval + serverTime), _maxRoundTime] remoteExec ["NikkoClient_script_NewRound", 0];//Round Start Client


true;