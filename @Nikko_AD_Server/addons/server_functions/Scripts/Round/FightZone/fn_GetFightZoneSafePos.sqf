/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/

//[] call NikkoServer_script_GetFightZoneSafePos
 

params[
	["_nearestObjectMinDistance",	1],
	["_minSurfaceNormal",			0.95],
	["_spawnZoneNearLimit",			1000]
];

_getNewPos = {
	private _newPos = [];
	private _isValid = false;
	while {!_isValid} do {
		_newPos = [nil, ["water"]] call BIS_fnc_randomPos;
		_isValid = ([_newPos, _minSurfaceNormal, _spawnZoneNearLimit] call isValidPosition)
	};
	_newPos
};

(call _getNewPos);  

