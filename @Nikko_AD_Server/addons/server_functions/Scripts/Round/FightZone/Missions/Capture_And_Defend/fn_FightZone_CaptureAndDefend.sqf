/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/

//Start
private _startupClient = {
	NikkoClient_script_missionSpawn = {
		params[["_location",[]]];
		if((count _location) > 0)then{
			private _oldPos = getPosASL player;
			player setVariable ["spawning",true,true];

			if(player call isAttacker)then{
				_location = getPosATL (selectRandom nearestObjects [_location, [], 1050]); 
			}else{
				_location = (getMarkerPos DefenderFlagMarker);
				_location = getPosATL (selectRandom nearestObjects [_location, [], 350]); 
			};

			player setPosATL _location;
			waitUntil{ not(_oldPos isEqualTo (getPosATL player))};
			player setVariable ["spawning",false,true];
		};
		true
	};

	player addWeapon "ItemMap";
	player addAction ["Jump to flag", {player setPosATL (getMarkerPos DefenderFlagMarker)},[],0,true,false,"","_this call isDeveloper && _this call isAttacker",2];

	if(player call isAttacker)then{
		 
	}else{
		
	};
};

private _startupServer = {
	//Get mission area
	private _missionLocation = ([1] + _this) call NikkoServer_script_GetFightZoneSafePos;
	private _missionRadius = 2500; 
    
	//Markers to create
	Nikko_Var_FightZoneMarkers pushBackUnique "FightZoneFlagDot";
	DefenderFlagMarker = createMarker ["FightZoneFlagDot", _missionLocation];//todo make random place in circle
	DefenderFlagMarker setMarkerType "mil_dot";
	DefenderFlagMarker setMarkerBrush "Solid";
	DefenderFlagMarker setMarkerText "Capture & Defend";

	//Objects to create
	DefendersFlag = createVehicle ["Flag_Blue_F", (getMarkerPos [DefenderFlagMarker,true]), [], 0, "CAN_COLLIDE"];
	DefendersFlag addAction ["Capture", {[player] remoteExec ["NikkoServer_script_FightZone_Capture",2]},[],0,true,false,"","_this call isAttacker",9.9];
  
	//Do any mission client here
	{[[], _startupClient] remoteExec ["call",owner _x]}forEach (Nikko_Var_Attackers + Nikko_Var_Defenders);
	
	//Return
	[_missionLocation, _missionRadius]
};

//Clean up
private _cleanupClient = {
	removeAllActions player;
};

private _cleanupServer = {
	//Delete Objects
	deleteVehicle DefendersFlag;

	//Do any mission client here
	{[[], _cleanupClient] remoteExec ["call",owner _x]}forEach (Nikko_Var_Attackers + Nikko_Var_Defenders);
};

_this call (if(typeName _this == "STRING")then{_cleanupServer}else{_startupServer});