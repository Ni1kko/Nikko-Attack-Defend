/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/
[player] spawn {
	params[
		"_caller"
	];
	
	private _flagPos = (getMarkerPos DefenderFlagMarker);
	private _callerPos = { (_flagPos distance2D _caller) };
 	private _getNearestDefender = {
		_nearestDefender = 0;
		{
			_d =  _flagPos distance2D _x;
			if(_d > _nearestDefender)then{
				_nearestDefender = _d;
			};
		}forEach Nikko_Var_Defenders;
		_nearestDefender
	};
	 
	if((call _callerPos) > 10) exitWith {
		"Round System: You Must be 10m Before Capturing" call doErrorMsg;
	};

	if((call _getNearestDefender) < 50) exitWith {
		"Round System: Kill All Players Before Capturing" call doErrorMsg;
	};

	private _captime = serverTime + 30;
	"Round System: Someone is Capturing" call doErrorMsg;
	
	waitUntil{_captime > serverTime || (call _getNearestDefender) < 50 || (call _callerPos) > 10};

	if((call _callerPos) > 10) exitWith {
		"Round System: You Must Stay Within 10m To Capture" call doErrorMsg;
	};

	if((call _getNearestDefender) < 50) exitWith {
		"Round System: Kill All Players Before Capturing" call doErrorMsg;
	};

	if(_captime > serverTime)then{
		"Round System: Someone Captured" call doErrorMsg;
	};
	
	
};



