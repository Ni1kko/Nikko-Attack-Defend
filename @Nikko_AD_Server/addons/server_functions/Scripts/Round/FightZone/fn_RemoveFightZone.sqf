/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/
if(isNil "FightZoneMission")exitWith{};

params [ 
	["_roundInterval", 45, [0]],
	["_maxRoundTime", 15, [0]]
];

//Update things
missionNamespace setVariable ["Nikko_Var_RoundInProgress", false, true];
missionNamespace setVariable ["Nikko_Var_RoundsCount",(Nikko_Var_RoundsCount + 1), true];
missionNamespace setVariable ["Nikko_Var_waitingRndEndMessageQueue",[], true];

"CLEANUP" call (missionNamespace getVariable [format["NikkoServer_script_FightZone_%1",FightZoneMission], {[]}]);

//switch sides
[] call switchRoundPlayers;

//Round End Client
[_roundInterval, _maxRoundTime] remoteExec ["NikkoClient_script_EndRound", 0];

//Delete Markers
{deleteMarker _x} forEach Nikko_Var_FightZoneMarkers;
missionNamespace setVariable ["Nikko_Var_FightZoneMarkers", [], true];

//Clear active mission
FightZoneMission = nil;

true