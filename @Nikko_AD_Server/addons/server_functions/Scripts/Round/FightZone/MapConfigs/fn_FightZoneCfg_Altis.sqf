/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/
 
// These configs are the default values from the main config. Just included here as an example.
private _SpawnZoneNearBlacklist	= 1500;

// Altis is pretty flat, so we can make the min surfaceNormal ... stricter? more strict? Who cares, you get the idea.
private _MinSurfaceNormal		= 0.95;

// Making these configs below as strict as possible will help in reducing the number of attempts taken to find a valid position, and as a result, improve performance.
private _MinDistFromWestBorder	= 2000;	// There's at least 2km of ocean from the west edge to the first bit of land.
private _MinDistFromEastBorder	= 2250;	// There's over 2km of ocean from the east edge to the first bit of land.
private _MinDistFromSouthBorder	= 5000;	// There's about 5km of ocean from the south edge to the first bit of land.
private _MinDistFromNorthBorder	= 5200;	// There's around 5km of ocean from the north edge to the first bit of land (including the island).
private _MinDistFromBorders 	= [_MinDistFromNorthBorder, _MinDistFromEastBorder, _MinDistFromSouthBorder, _MinDistFromWestBorder];

//
private _allowedMissions = [
	"CaptureAndDefend"
];

[_SpawnZoneNearBlacklist, _MinSurfaceNormal, _MinDistFromBorders, _allowedMissions]
