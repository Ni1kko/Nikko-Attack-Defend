
/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/
if(missionNamespace getVariable ["Nikko_Var_RoundSystemOnline", false])exitWith{};
missionNamespace setVariable ["Nikko_Var_RoundSystemOnline", true];

//TODO: made cfg
private _roundInterval = 20;//in secs
private _maxRoundTime = 1;//in mins

//Pre setup... Declare defaults
missionNamespace setVariable ["Nikko_Var_connnectedPlayers", [], true];
missionNamespace setVariable ["Nikko_Var_Attackers", [], true];
missionNamespace setVariable ["Nikko_Var_Defenders", [], true];
missionNamespace setVariable ["Nikko_Var_RoundInProgress", false, true];
missionNamespace setVariable ["Nikko_Var_RoundsCount", 0, true];
missionNamespace setVariable ["Nikko_Var_FightZoneMarkers", [], true];
missionNamespace setVariable ["Nikko_Var_roundTime", serverTime, true];
missionNamespace setVariable ["Nikko_Var_selectedMissionLocation", [], true];
missionNamespace setVariable ["Nikko_Var_waitingRndEndMessageQueue", [], true];
missionNamespace setVariable ["NikkoClient_script_missionSpawn", {
	params[["_location",Nikko_Var_selectedMissionLocation]];
	if((count _location) > 0)then{
		private _oldPos = getPosASL player;
		player setVariable ["spawning",true,true];
		_location = getPosATL (selectRandom nearestObjects [_location, [], 350]);
		player setPosATL _location;
		waitUntil{ not(_oldPos isEqualTo (getPosATL player))};
		player setVariable ["spawning",false,true];
	}; 
}, true];
 
isAttacker = { (_this getVariable ["side",""]) == "Attacker"};
isDefender = { (_this getVariable ["side",""]) == "Defender"};

//Get connected players from requested group
getRoundPlayers = compileFinal '
	params [ [ "_side", -1, [0] ] ];
	private _players = [];
	if !(_side in [0,1]) exitWith {
		{
			private _player = _x;
			if(getPlayerUID _x in Nikko_Var_connnectedPlayers)then{
				_side = (if(count Nikko_Var_Defenders > count Nikko_Var_Attackers)then {"Attacker"}else{"Defender"});
				_player setVariable ["side",_side,true];
				_players pushBackUnique _player;
			};
		}forEach allPlayers;
		_players
	};
	_side = (if (_side == 1) then {"Attacker"}else{"Defender"});
	{
		private _player = _x;
		if((_player getVariable ["side",""]) isEqualTo _side)then{
			_players pushBackUnique _player;	
		};
	}forEach allPlayers;
	_players;
';

//Switch connected players group
switchRoundPlayers = compileFinal ' 
	private _defenders = [0] call getRoundPlayers;	
	private _attackers = [1] call getRoundPlayers;
	{if((getPlayerUID _x) in Nikko_Var_connnectedPlayers) then {_x setVariable ["side","Attacker"]; [[_x], Nikko_Group_Attackers] remoteExec ["joinSilent", owner _x]}}forEach _defenders;
	{if((getPlayerUID _x) in Nikko_Var_connnectedPlayers) then {_x setVariable ["side","Defender"]; [[_x], Nikko_Group_Defenders] remoteExec ["joinSilent", owner _x]}}forEach _attackers;
	true
';

//Get alive connected players
aliveRoundPlayers = compileFinal '
	params [ ["_side", -1, [0]] ];
	if(overrideRoundPlayersNeeded)exitWith{true};
	if !(_side in [0,1]) exitWith {false};
	private _alivePlayers = [];
	{if(alive _x) then {_alivePlayers pushBackUnique _x}}forEach ([_side] call getRoundPlayers);
	count _alivePlayers > 0
';

//
isValidPosition = {
    if !(params["_pos", "_minSurfaceNormal", "_spawnZoneNearLimit"])exitWith{};
    private _isValidPos = false;

    try
    {
        if ((count _pos)<2) throw ("(UNDEFINED POSITION)");
        if ((count _pos) isEqualTo 2) then {_pos set [2, 0];};
        //if ([_pos, PosBlacklist] call IsPosBlacklisted) throw "a blacklisted position";
  
        // Terrain steepness check
        // 0 surfacenormal means completely vertical, 1 surfaceNormal means completely flat and horizontal.
        // Take the arccos of the surfaceNormal to determine how many degrees it is from the horizontal. In SQF: {acos ((surfaceNormal _pos) select 2)}. Don't forget to define _pos.
        if ((_minSurfaceNormal>0) && {_minSurfaceNormal<=1}) then
        {
            if (((surfaceNormal _pos) select 2)<_minSurfaceNormal) throw "a steep location";

            // Check the surrounding area (within 5 meters) 
            for "_dir" from 0 to 359 step 45 do
            {
                if (((surfaceNormal (_pos getPos [5,_dir])) select 2)<_minSurfaceNormal) throw "a nearby steep location";
            };
        };

        //Marker distance check
        {
            private _blacklistedByDistance = (_x in ["respawn_guerrila","AttackerSpawn","DefenderSpawn"]);

            if(_blacklistedByDistance)then
            {
                // Check for nearby spawn points
                if (((getMarkerPos _x) distance2D _pos) <= _spawnZoneNearLimit) throw "a spawn zone";
            };
            
        } forEach allMapMarkers;

        // No exceptions found
        _isValidPos	= true;
    }
    catch
    {   
        _isValidPos = false;
    };

    _isValidPos
};

//
getRoundTimeLeft = { 
	params [["_rndSecs", 0, [0]],["_text", true]];
	private _rndHrs = floor(_rndSecs / 60 / 60);  
	private _rndMins = ((_rndSecs / 60 / 60) - _rndHrs);  
	_rndMins = floor((if(_rndMins == 0)then{0.0001} else {_rndMins}) * 60); 
	if _text exitWith {format (if(_rndHrs > 0)then{["[%1 Hrs %2 Mins]", _rndHrs,_rndMins]}else{if(_rndMins > 0.0001)then{["[%1 Mins]", _rndMins]}else{["[%1 Seconds]", _rndSecs]}})};
	if(_rndHrs > 0)then{
		_num = str _rndHrs + "." + str _rndMins;
		(call compile _num) toFixed 2
	}else{
		if(_rndMins > 0.0001)then{_rndMins}else{_rndSecs}
	};
};

//
getRoundVedict = {
	_message = "";
	switch (true) do {
		case not([0] call aliveRoundPlayers):    { _message = "Defenders"; };
		case not([1] call aliveRoundPlayers):    { _message = "Attackers"; };
		case (serverTime > Nikko_Var_roundTime): { _message = "Time"; };
		case (false): 	   						 { _message = "Captured"; };//To be added
	};
	_message
};

//Override players need for round start
startRoundWithoutPlayers = {
	params [["_a",false]];
	if(!_a)exitwith {missionNamespace setVariable ["overrideRoundPlayersNeeded", true, true]};
	[]spawn {
		while{count Nikko_Var_connnectedPlayers < 2} do{
			waitUntil {!overrideRoundPlayersNeeded};
			missionNamespace setVariable ["overrideRoundPlayersNeeded", true, true];
		};
	};
};
 
//Add Connection Event Handlers
NikkoServer_var_ClientConnectedRound = addMissionEventHandler ['PlayerConnected', {[_this#1,_this#2,_this#3,_this#4] spawn NikkoServer_script_onplayerconnectedRound}];
NikkoServer_var_ClientDisconnectedRound = addMissionEventHandler ['PlayerDisconnected', {[_this#1,_this#2,_this#3,_this#4] spawn NikkoServer_script_onplayerdisconnectedRound}];

//Create Attacker Group
missionNamespace setVariable ["Nikko_Group_Attackers", (createGroup west), true];
private _Attacker = Nikko_Group_Attackers createUnit [ "I_Survivor_F", [0,0,100], [], 0, "FORM"];
Nikko_Group_Attackers setGroupIdGlobal ["A&D | Attackers"];

//Create Defender Group
missionNamespace setVariable ["Nikko_Group_Defenders", (createGroup east), true];
private _Defender = Nikko_Group_Defenders createUnit [ "I_Survivor_F", [0,0,100], [], 0, "FORM"];
Nikko_Group_Defenders setGroupIdGlobal ["A&D | Defenders"];

["startRoundWithoutPlayers", "getRoundTimeLeft", "isAttacker", "isDefender", "isValidPosition", "getRoundVedict"] call compileGlobal;

//Global group options
{
    _x enableSimulation false;
    _x allowDamage false;
    [_x] remoteExecCall ["hideObject",-2,true];
} forEach [_Attacker, _Defender];

"Round System Started" call doErrorMsg;

//Start new round
[_roundInterval, _maxRoundTime] spawn NikkoServer_script_NewRound;