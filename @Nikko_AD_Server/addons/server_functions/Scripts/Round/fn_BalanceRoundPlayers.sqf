/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/
if(missionNamespace getVariable ["Nikko_Var_RoundInProgress", false])exitWith{};

private _attackers = count Nikko_Var_Attackers;
private _defenders = count Nikko_Var_Defenders;

private _isFinished = false;
while {(_attackers < _defenders || _attackers > _defenders) && !_isFinished} do 
{
	if(_attackers + 1 == _defenders || _defenders + 1 == _attackers)exitWith{_isFinished = true;};
	"Round System: Balancing players" call doErrorMsg;
	private _target = selectRandom (if(_attackers < _defenders)then{Nikko_Var_Defenders}else{Nikko_Var_Attackers});
	private _targetGrp = (if(_attackers < _defenders) then {Nikko_Group_Defenders} else {Nikko_Group_Attackers});
	private _message = format [(if(_attackers < _defenders)then{"Too Many Defenders: Moving (%1) Onto Attackers"}else{"Too Many Attackers: Moving (%1) Onto Defenders"}), name _target];
	_message = format ["(Auto Balance Teams) %1", _message];
	diag_log _message;
	_message remoteExec ["systemChat", 0];
	[[_target], _targetGrp] remoteExec ["joinSilent", owner _target];
	missionNamespace setVariable ["Nikko_Var_Attackers", ([1] call getRoundPlayers), true];
	missionNamespace setVariable ["Nikko_Var_Defenders", ([0] call getRoundPlayers), true];
	_attackers = count Nikko_Var_Attackers;
    _defenders = count Nikko_Var_Defenders;
};