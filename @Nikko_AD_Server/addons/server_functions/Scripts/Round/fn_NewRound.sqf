
/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/
if(missionNamespace getVariable ["Nikko_Var_RoundInProgress", false])exitWith{};
 
params [ 
	["_roundInterval", 45, [0]],
	["_maxRoundTime", 15, [0]]
];

//Debug
if(!isDedicated)then{
	"Round System: Player override active" call doErrorMsg;
	[true] call startRoundWithoutPlayers;
};

//Wait till enough players
missionNamespace setVariable ["overrideRoundPlayersNeeded", false, true];

"Round System: Player wait" call doErrorMsg;
waitUntil{ uiSleep 0.1; (count(call getRoundPlayers) >= (if(overrideRoundPlayersNeeded)then{1}else{2}))};
"Round System: Player wait passed" call doErrorMsg;

//Get this rounds groups
missionNamespace setVariable ["Nikko_Var_Attackers", ([1] call getRoundPlayers), true];
missionNamespace setVariable ["Nikko_Var_Defenders", ([0] call getRoundPlayers), true];

//Auto Balance Teams
[] call NikkoServer_script_BalanceRoundPlayers;

//Round Start
[_roundInterval, _maxRoundTime] call NikkoServer_script_NewFightZone;

//wait for round vedict
format ["(Round#%1) Waiting For Round Verdict", Nikko_Var_RoundsCount] call doErrorMsg;
waitUntil { uiSleep 0.1; (call getRoundVedict) != ""};

//Log round vedict
switch (call getRoundVedict) do {
	case "Defenders":  { format ["(Round#%1 WON) Defenders Died", Nikko_Var_RoundsCount] call doErrorMsg;};
	case "Attackers":  { format ["(Round#%1 WON) Attackers Died", Nikko_Var_RoundsCount] call doErrorMsg;};
	case "Time": 	   { format ["(Round#%1 END) Time Reached",   Nikko_Var_RoundsCount] call doErrorMsg;};
	case "Captured":   { format ["(Round#%1 END) Flag Captured",  Nikko_Var_RoundsCount] call doErrorMsg;};
};

uiSleep 5;

//update things
[_roundInterval, _maxRoundTime] call NikkoServer_script_RemoveFightZone;
  
//loop
[_roundInterval, _maxRoundTime] spawn NikkoServer_script_NewRound;
