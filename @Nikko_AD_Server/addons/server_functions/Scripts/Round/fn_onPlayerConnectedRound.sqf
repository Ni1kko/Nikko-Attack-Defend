/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/

params [
	['_uid','',['']],
	['_name','',['']],
	['_jip',false,[false]],
	['_owner',2,[0]]
];

Nikko_Var_connnectedPlayers pushBackUnique _uid;

["side", (if(count Nikko_Var_Defenders > count Nikko_Var_Attackers)then{"Attacker"}else{"Defender"}), true] remoteExec ["setVariable", _owner];

//Return
true
