/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/
params[
    ["_clientData",[[],false],[[]]],
    ["_serverCommandPass","",[""]],
	["_isLiveServer",true,[false]]
];

doErrorMsg = {
	if(typeName _this != "STRING")exitWith{};
	diag_log _this;
	if(isServer && !is3DENMultiplayer)exitWith{
		_this remoteExec ["doErrorMsg",2];
	};
	_this remoteExec ["systemChat",0];
};

selectRandomValue = {
	if !(params["_min","_max"])exitWith{};
	_min + random(_max - _min)
};

//["var"] call compileGlobal;
compileGlobal = {
	if(typeName _this != "ARRAY") then{_this = [_this]};
	{
		private _value = missionNamespace getVariable [_x,nil];
		if(typeName _value == "CODE") then{
			_value = str(_value);
			_value = _value select [1, count(_value)-2];
		};
		if(typeName _value != "STRING") then{_value = str(_value)};
		missionNamespace setVariable [_x,compileFinal _value, true];
	}forEach _this;
};
 
//player call isDeveloper
isDeveloper = {
	params [["_player",objNull]];
	if(!hasInterface)exitWith{true};
    if(isNull _player)exitWith{false};
	if(getPlayerUID _player in ([missionConfigFile, "enableDebugConsole",[]] call BIS_fnc_returnConfigEntry))exitWith{true};
	false;
};

[
	"compileGlobal",
	"doErrorMsg",
    "isDeveloper",
	"selectRandomValue"
] call compileGlobal;

//Start ExtDB3
[] call NikkoServer_script_initDatabase;
waitUntil{
    (call (uiNamespace getVariable ["NikkoServer_var_DBOnline",{false}]) || !_isLiveServer)
};

//Add Connection Event Handlers
NikkoServer_var_ClientConnected = call compile ("addMissionEventHandler ['PlayerConnected', {[_this#1,_this#2,_this#3,_this#4,"+str(_clientData)+","+str(_isLiveServer)+"] spawn NikkoServer_script_onplayerconnected}]");
NikkoServer_var_ClientDisconnected = call compile ("addMissionEventHandler ['PlayerDisconnected', {[_this#1,_this#2,_this#3,_this#4] spawn NikkoServer_script_onplayerdisconnected}]");
 
if ((is3DEN || is3DENMultiplayer) AND !isMultiplayer) then {
    ["3DEN",profileName,true,2,_clientData,_isLiveServer] spawn NikkoServer_script_onplayerconnected;
};

[]spawn{
    private _players = [];
    while {1==1} do {
        waitUntil{ uiSleep 0.1; (count allPlayers > 0) };
        {
            if(!(_x in _players))then{
                [[],{
                    systemChat "Server Loaded";
                }]remoteExec ["call",owner _x];
                _players pushBackUnique _x;
            };
            uiSleep 0.1;
        } forEach allPlayers;
        uiSleep 0.3;
    };
};

[] spawn NikkoServer_script_initRoundSystem;

diag_log "Server started";

//Server Ready => UNLOCK
_serverCommandPass serverCommand "#unlock";