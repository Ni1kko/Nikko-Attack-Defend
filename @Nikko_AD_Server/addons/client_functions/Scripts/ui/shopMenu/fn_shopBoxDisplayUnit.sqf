/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/
params [
	["_loadout",[],[[]]]
];

if (!isNull (missionNamespace getVariable ["ShopBoxWeapHolder",objNull])) then {deleteVehicle (missionNamespace getVariable ["ShopBoxWeapHolder",objNull]);};
if (!isNull (missionNamespace getVariable ["ShopBoxWeapTurnItem",objNull])) then {deleteVehicle (missionNamespace getVariable ["ShopBoxWeapTurnItem",objNull]);};

//Create turn item that we will later attach unit too
missionNamespace setVariable ["ShopBoxWeapTurnItem",("Land_Can_V3_F" createVehicleLocal getPosASL (missionNamespace getVariable ["ShopBoxTable",objNull]))];
(missionNamespace getVariable ["ShopBoxWeapTurnItem",objNull]) setPosATL getPosASL (missionNamespace getVariable ["ShopBoxTable",objNull]);
(missionNamespace getVariable ["ShopBoxWeapTurnItem",objNull]) attachTo [(missionNamespace getVariable ["ShopBoxTable",objNull]), [0, 0, 0] ];
(missionNamespace getVariable ["ShopBoxWeapTurnItem",objNull]) hideObject true;
detach (missionNamespace getVariable ["ShopBoxWeapTurnItem",objNull]);

//Temp Adjust camera as i cba making a new shop box for units
if (!isNull (missionNamespace getVariable ["ShopBoxCamera",objNull])) then {
	(missionNamespace getVariable ["ShopBoxCamera",objNull]) camSetFov 1.5;
	(missionNamespace getVariable ["ShopBoxCamera",objNull]) camCommit 0;
};

//Create a local unit in shop view
missionNamespace setVariable ["ShopBoxWeapHolder",("B_RangeMaster_F" createVehicleLocal getPosASL (missionNamespace getVariable ["ShopBoxTable",objNull]))];

//strip local unit
removeAllWeapons (missionNamespace getVariable ["ShopBoxWeapHolder",objNull]);
removeUniform (missionNamespace getVariable ["ShopBoxWeapHolder",objNull]);
removeBackPack (missionNamespace getVariable ["ShopBoxWeapHolder",objNull]);
removeVest (missionNamespace getVariable ["ShopBoxWeapHolder",objNull]);
removeHeadGear (missionNamespace getVariable ["ShopBoxWeapHolder",objNull]);
removeAllAssignedItems (missionNamespace getVariable ["ShopBoxWeapHolder",objNull]);
removeGoggles (missionNamespace getVariable ["ShopBoxWeapHolder",objNull]);
 
//Add each loadout item to the local created unit
{
	private _loadoutItems = _x;
	{
		private _item = _x;
		if (_item != "") then {

			private _itemInfo = [_item] call NikkoClient_script_itemDetails;
			private _category = _itemInfo#4;
			private _type = _itemInfo#5;

			if (_category != "") then{
				switch (_category) do {
					//weapons are the most valuable, therefore, we do not allow disappearance and replacement
					case "Weapon": {
						private _config = configFile >> "CfgWeapons" >> _item;
						if (isClass _config OR (getNumber (_config >> "scope") > 0)) then{
							// can we add directly to the hands
							private _canAdd = switch (getNumber (_config >> "type")) do {
								case (1): {(primaryWeapon ShopBoxWeapHolder) == ""};
								case (2): {(handgunWeapon ShopBoxWeapHolder) == ""};
								case (4): {(secondaryWeapon ShopBoxWeapHolder) == ""};
								default {false};
							};

							// add if the desired slot is free
							if (_canAdd) then {
								ShopBoxWeapHolder addWeapon _item; 
							};
						};
					};
					// we have simpler clothes, the main thing is to save the contents
					case "Equipment": {
						switch (_type) do {
							case "Glasses": {
								ShopBoxWeapHolder addGoggles _item;
							};
							case "Headgear": {
								ShopBoxWeapHolder addHeadGear _item;
							};
							case "Vest": {
								ShopBoxWeapHolder addVest _item;
							};
							case "Uniform": {
								ShopBoxWeapHolder forceAddUniform _item;
							};
							case "Backpack": {
								ShopBoxWeapHolder addBackpack _item;
							};
							default {};
						};
					};
					// now with all sorts of trash  
					case "Item": {
						switch (true) do {
							//bipod, sights, ect
							case (_type in ["AccessoryMuzzle","AccessoryPointer","AccessorySights","AccessoryBipod"]) : {

								private _weapon = switch (true) do {
									case (_item in ([(primaryWeapon ShopBoxWeapHolder)] call NikkoClient_script_compatibleItems)) : {"rifle"};
									case (_item in ([(handgunWeapon ShopBoxWeapHolder)] call NikkoClient_script_compatibleItems)) : {"pistol"};
									case (_item in ([(secondaryWeapon ShopBoxWeapHolder)] call NikkoClient_script_compatibleItems)) : {"launcher"};
									default {""};
								};

								switch (_weapon) do {
									case "rifle" : {
										if !(_item in (primaryWeaponItems ShopBoxWeapHolder)) then {
											ShopBoxWeapHolder addPrimaryWeaponItem _item;
										};
									}; 
									case "pistol" : {
										if !(_item in (handgunItems ShopBoxWeapHolder)) then {
											ShopBoxWeapHolder addHandgunItem _item;
										};
									}; 
									case "launcher" : {
										if !(_item in (secondaryWeaponItems ShopBoxWeapHolder)) then {
											ShopBoxWeapHolder addSecondaryWeaponItem _item;
										};
									};
									default {}; 
								};
							};
							//map, binoculars, etc.
							case (_type in ["Compass","GPS","Map","Radio","Watch","NVGoggles","UAVTerminal","MineDetector","LaserDesignator","Binocular"]) : {
								switch (true) do {
									case (ShopBoxWeapHolder canAddItemToBackpack _item) : {
										ShopBoxWeapHolder addItemToBackpack _item;
									};
									case (ShopBoxWeapHolder canAddItemToVest _item) : {
										ShopBoxWeapHolder addItemToVest _item;
									}; 
									case (ShopBoxWeapHolder canAddItemToUniform _item) : {
										ShopBoxWeapHolder addItemToUniform _item;
									};
									default {};
								};
								ShopBoxWeapHolder linkItem _item;
							}; 
							default {};
						};
					}; 
					default {};
				};
			};
		};
	}forEach _loadoutItems;
}forEach _loadout;

//Attach unit too turn item
(missionNamespace getVariable ["ShopBoxWeapHolder",objNull]) attachTo [(missionNamespace getVariable ["ShopBoxWeapTurnItem",objNull]), [0,0,0.4]]; 
(missionNamespace getVariable ["ShopBoxWeapHolder",objNull]) setVectorDirAndUp [[-0,-9,-0.2],[0,-1,0]];