/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/
params [
	["_item","",[""]],
	["_itemCategory","",[""]]
];

if (!isNull (missionNamespace getVariable ["ShopBoxWeapHolder",objNull])) then {deleteVehicle (missionNamespace getVariable ["ShopBoxWeapHolder",objNull]);};
if (!isNull (missionNamespace getVariable ["ShopBoxWeapTurnItem",objNull])) then {deleteVehicle (missionNamespace getVariable ["ShopBoxWeapTurnItem",objNull]);};

//Create turn item that we will later attach unit too
missionNamespace setVariable ["ShopBoxWeapTurnItem",("Land_Can_V3_F" createVehicleLocal getPosASL (missionNamespace getVariable ["ShopBoxTable",objNull]))];
(missionNamespace getVariable ["ShopBoxWeapTurnItem",objNull]) setPosATL getPosASL (missionNamespace getVariable ["ShopBoxTable",objNull]);
(missionNamespace getVariable ["ShopBoxWeapTurnItem",objNull]) attachTo [(missionNamespace getVariable ["ShopBoxTable",objNull]), [0, 0, 0] ];
(missionNamespace getVariable ["ShopBoxWeapTurnItem",objNull]) hideObject true;
detach (missionNamespace getVariable ["ShopBoxWeapTurnItem",objNull]);

//Create a local weaponholder in shop view
missionNamespace setVariable ["ShopBoxWeapHolder",("WeaponHolderSimulated" createVehicleLocal getPosASL (missionNamespace getVariable ["ShopBoxTable",objNull]))];
switch (_itemCategory) do {
	case "Weapon": {
		(missionNamespace getVariable ["ShopBoxWeapHolder",objNull]) addWeaponCargo [_item, 1];
	};
	case "Mine";
	case "Magazine": {
		(missionNamespace getVariable ["ShopBoxWeapHolder",objNull]) addMagazineCargo [_item, 1];
	};
	default {
		(missionNamespace getVariable ["ShopBoxWeapHolder",objNull]) addItemCargo [_item, 1];
	};
};

//Attach weaponholder too turn item
(missionNamespace getVariable ["ShopBoxWeapHolder",objNull]) attachTo [(missionNamespace getVariable ["ShopBoxWeapTurnItem",objNull]), [0,-0.63,0.7]];
(missionNamespace getVariable ["ShopBoxWeapHolder",objNull]) setVectorDirAndUp [[0,0,1],[0,-1,0]];