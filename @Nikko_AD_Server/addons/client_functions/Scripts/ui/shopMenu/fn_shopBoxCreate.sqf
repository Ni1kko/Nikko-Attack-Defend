/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/

ShopBoxPosition = [0,0,300];
ShopBoxWeapHolder = objNull;
ShopBoxWeapTurnItem = objNull;

ShopBoxBackgroundObject = "Land_i_Shop_02_V1_F" createVehicleLocal ShopBoxPosition;
ShopBoxBackgroundObject enableSimulation false;
ShopBoxBackgroundObject allowDamage false;
ShopBoxBackgroundObject setPosASL ShopBoxPosition;
ShopBoxBackgroundObject setDir (getDir player);

ShopBoxTable = "Land_TableDesk_F" createVehicleLocal ShopBoxPosition;
ShopBoxTable enableSimulation false;
ShopBoxTable allowDamage false;
ShopBoxTable attachTo [ShopBoxBackgroundObject, [-1, -2, -2.25] ];
detach ShopBoxTable;
ShopBoxTable setDir ((getDir ShopBoxBackgroundObject) - 90);

ShopBoxLightSource = "#lightpoint" createVehicleLocal [0,0,0];
ShopBoxLightSource lightAttachObject [ShopBoxTable,[0,-1,1]];
ShopBoxLightSource setLightColor [255,255,255];
ShopBoxLightSource setLightAmbient [1,1,0.2];
ShopBoxLightSource setLightAttenuation [0,0,5,0];
ShopBoxLightSource setLightIntensity 20;
ShopBoxLightSource setLightUseFlare true;
ShopBoxLightSource setLightFlareSize 0;
ShopBoxLightSource setLightFlareMaxDistance 50;

ShopBoxCameraCord = ShopBoxTable modelToWorld [-0.3,-1.5,0.8];
ShopBoxTableCord = ShopBoxTable modelToWorld [-0.3,0,0.8];

ShopBoxCamera = "camera" camCreate ShopBoxCameraCord;
ShopBoxCamera cameraEffect ["External","BACK"];
ShopBoxCamera camSetPos ShopBoxCameraCord;
ShopBoxCamera camSetDir (ShopBoxCameraCord vectorFromTo ShopBoxTableCord);
ShopBoxCamera camSetFov 0.7;
ShopBoxCamera camCommit 0;

showCinemaBorder false;
showChat false;

//Remove Shopbox eventhandlers (should of been removed on display destory but just incase)
{
	(findDisplay 38400) displayRemoveAllEventHandlers _x;	
}forEach [
	"MouseButtonDown","MouseButtonUp","MouseMoving"
];

//Add new hanlders to allowing moving the item they are viewing
(findDisplay 38400) displayAddEventHandler ["MouseButtonDown",{
	uiNamespace setVariable ["NikkoClient_var_shopviewing",(_this#5)];
}];
(findDisplay 38400) displayAddEventHandler ["MouseButtonUp",{
	uiNamespace setVariable ["NikkoClient_var_shopviewing",false];
}];
(findDisplay 38400) displayAddEventHandler ["MouseMoving",{
	if(uiNamespace getVariable ["NikkoClient_var_shopviewing",false])then{
		private _posMin = -360;
		private _posMax = 360;
		private _posAdj = 0;

		if((_this#1) > 0)then{
			_posAdj = ((uiNamespace getVariable ["NikkoClient_var_shopviewAdj",0]) + 5)
		}else{
			_posAdj = ((uiNamespace getVariable ["NikkoClient_var_shopviewAdj",0]) - 5)
		};

		if(_posAdj > _posMax)then{_posAdj = _posMax};
		if(_posAdj < _posMin)then{_posAdj = _posMin};

		uiNamespace setVariable ["NikkoClient_var_shopviewAdj",_posAdj];

		ShopBoxWeapTurnItem setDir _posAdj;
	};
}];