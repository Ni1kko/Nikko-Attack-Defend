/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/

private _index = param [1,-1,[0]];
if (_index == -1) exitWith {systemChat "Bad Data Filter"; closeDialog 0;}; //Bad data passing.
if (!isNull (missionNamespace getVariable ["ShopBoxWeapHolder",objNull])) then {deleteVehicle (missionNamespace getVariable ["ShopBoxWeapHolder",objNull]);};//delete out holder on filter change

private _shop = (if(missionNamespace getVariable ["NikkoClient_var_isAttacking",false])then{"Atackers"}else{"Defenders"});
if !(isClass(missionConfigFile >> "NikkoClient_CFG_Shops" >> _shop)) exitWith {systemChat "Bad Data Filter"; closeDialog 0;};

uiNamespace setVariable ["Weapon_Shop_Filter",_index];

private _itemList = ((findDisplay 38400) displayCtrl 38403);
lbClear _itemList;

((findDisplay 38400) displayCtrl 38405) ctrlSetText (["Purchase","Sell","Purchase Again"]#_index);//Buy-sell btn
((findDisplay 38400) displayCtrl 38406) ctrlShow (_index in [0,1]);//Mags box
((findDisplay 38400) displayCtrl 38407) ctrlShow false;
((findDisplay 38400) displayCtrl 38408) ctrlShow false;
((findDisplay 38400) displayCtrl 38409) ctrlShow false;
((findDisplay 38400) displayCtrl 38410) ctrlShow false;
((findDisplay 38400) displayCtrl 38404) ctrlSetStructuredText parseText "";

//Temp Adjust camera as i cba making a new shop box for units
if (!isNull (missionNamespace getVariable ["ShopBoxCamera",objNull])) then {
	(missionNamespace getVariable ["ShopBoxCamera",objNull]) camSetFov ([1.5,0.7] select (_index in [0,1]));
	(missionNamespace getVariable ["ShopBoxCamera",objNull]) camCommit 0;
};

switch (_index) do {
	case 0: {//shop inv
		{
			//is an item? 
			if ((_x#1) > 0) then {
				//get item info
				private _itemInfo = [_x#0] call NikkoClient_script_itemDetails;

				//any info?
				if(count _itemInfo > 0)then{
					_itemList lbAdd format["%1",_itemInfo#1];
					_itemList lbSetTextRight [(lbSize _itemList)-1, format["  %1 Warpoints", [_x#1] call NikkoClient_script_numberSafe]];
					_itemList lbSetValue[(lbSize _itemList)-1,_x#1];
					_itemList lbSetData[(lbSize _itemList)-1,_itemInfo#0];
					_itemList lbSetPicture[(lbSize _itemList)-1,_itemInfo#2];
				}; 
			};
		} foreach ([missionConfigFile >> "NikkoClient_CFG_Shops" >> _shop, "items" , []] call BIS_fnc_returnConfigEntry);
	};

	case 1: {//player inv
		private _playerInv = [];

		//Gear
		{
			if(_x != "") then {_playerInv pushBack _x};
		}forEach [
			(headgear player),
			(goggles player),
			(uniform player),
			(primaryWeapon player),
			(secondaryWeapon player),
			(handgunWeapon player)
		];

		//Items
		{
			private _items = _x;
			{if(_x != "")then{_playerInv pushBack _x}} forEach _items;
		}forEach [
			(primaryWeaponItems player),
			(assignedItems player),
			(uniformItems player),
			(vestItems player),
			(backPackItems player)
		];
		
		{
			//get item info
			private _itemInfo = [_x] call NikkoClient_script_itemDetails;

			//any info?
			if(count _itemInfo > 0)then{
				_itemCount = { _x == (_itemInfo#0)} count _playerInv;
				if (_itemCount > 1) then {
					_itemList lbAdd format["[%2] %1",_itemInfo#1,_itemCount];
				} else {
					_itemList lbAdd format["%1",_itemInfo#1];
				};
				_itemList lbSetData[(lbSize _itemList)-1,_itemInfo#0];
				_itemList lbSetPicture[(lbSize _itemList)-1,_itemInfo#2];
			};
		} foreach _playerInv;//all player items
	};

	case 2: {//previous inv
		private _listedLoadouts = [];
		{ 
			private _loadout = _x; 
			if !(_loadout in _listedLoadouts) then { //No duplicates or emptys
				//No duplicates		
				_listedLoadouts pushBack _loadout;

				private _price = 0;
				{ 
					private _loadoutItems = _x; 
					{
						private _loadoutItem = _x; 
						{if ((_x#0) == _loadoutItem) exitWith {_price = _price + (_x#1);}} foreach ([missionConfigFile >> "NikkoClient_CFG_Shops" >> _shop, "items" , []] call BIS_fnc_returnConfigEntry);
					} forEach _loadoutItems;
				} forEach _loadout;
				
				if(_price > 0)then {
					_itemList lbAdd format["Previous loadout %1",(_forEachIndex + 1)];
					_itemList lbSetData[(lbSize _itemList)-1,str _loadout];
					_itemList lbSetTextRight [(lbSize _itemList)-1, format["  %1 Warpoints", [_price] call NikkoClient_script_numberSafe]];
					_itemList lbSetValue[(lbSize _itemList)-1,_price];
					//_itemList lbSetPicture[(lbSize _itemList)-1,"textures\previousloadout.paa"];		
				};
			};
		} foreach (profileNamespace getVariable [format["NikkoClient_var_previous%1Inv",_shop],[]]);
	};
};

//Slect first item on filter change
if(lbSize _itemList > 0 || isNil {_this#0}) then {
	_itemList lbSetCurSel 0;
};