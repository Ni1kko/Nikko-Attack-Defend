/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/

params [ 
	['_resource', 'Nikko_Rsc_LoadingScreen', ['']],
	['_text', '', ['']]
];

if( not(call BIS_fnc_isLoading))then
{ 
	disableSerialization;  
	startLoadingScreen [_text, _resource];
};

[(call BIS_fnc_isLoading), 0];
 