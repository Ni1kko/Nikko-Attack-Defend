/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/

params [
	['_HeaderText','',['']],
	['_BodyText','',['']] 
];

if(isNull(uiNamespace getVariable ["Nikko_Rsc_LoadingScreen",displayNull]))exitWith{
	_this spawn {
		['Nikko_Rsc_LoadingScreen'] call NikkoClient_script_createLoadingScreen;
		uiSleep 0.5;
		_this call NikkoClient_script_updateLoadingScreen;
	};
};

disableSerialization;

((uiNamespace getVariable "Nikko_Rsc_LoadingScreen") displayCtrl 100) ctrlSetStructuredText parseText (format[
	"<t size='%1' color='%2'>%5</t><br/><t size='%3' color='%4'>%6</t>",
	([missionConfigFile >> 'Nikko_Rsc_LoadingScreen' >> 'Controls' >> 'MessageQueue','headerTextSize',3.0]call BIS_fnc_returnConfigEntry),
	([missionConfigFile >> 'Nikko_Rsc_LoadingScreen' >> 'Controls' >> 'MessageQueue','headerTextColor','#222']call BIS_fnc_returnConfigEntry),
	([missionConfigFile >> 'Nikko_Rsc_LoadingScreen' >> 'Controls' >> 'MessageQueue','bodyTextSize',1.5]call BIS_fnc_returnConfigEntry),
	([missionConfigFile >> 'Nikko_Rsc_LoadingScreen' >> 'Controls' >> 'MessageQueue','bodyTextColor','#fff']call BIS_fnc_returnConfigEntry),
	(_HeaderText), 
	(_BodyText)
]);

true; 
 