/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/

params [['_delay', 0, [0]]];
	
if(_delay > 0) then{ 
	if( not(canSuspend))exitwith{
		_delay spawn NikkoClient_script_destroyLoadingScreen;
	};
	uiSleep(_delay);
	endLoadingScreen;  
}else{
	endLoadingScreen; 
}; 

true