/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/

if !(player call isDeveloper)exitWith{
	[('We Appoligize ' +profilename+ ' An error occured'),format['%1<br/>server will now force disconnect you!',_this]] call NikkoClient_script_updateLoadingScreen;    
	10 spawn { 
		(_this) call NikkoClient_script_destroyLoadingScreen;
		uiSleep (_this);  
		(uiNamespace getVariable 'RscDisplayMission') closeDisplay 0; 
	};
	true
};

[('An error occured'),format['%1',_this]] call NikkoClient_script_updateLoadingScreen;
5 call NikkoClient_script_destroyLoadingScreen;

true