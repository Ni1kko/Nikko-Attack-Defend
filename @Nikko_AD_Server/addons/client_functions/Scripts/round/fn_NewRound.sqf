
/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/
if !(missionNamespace getVariable ["Nikko_Var_RoundInProgress", true])exitWith{};//not server that called

params [ 
	["_roundInterval", 0, [0]],
	["_maxRoundTime", 0, [0]]
];

//scaled in mins
_maxRoundTime = (_maxRoundTime * 60);

//Spawn in side location
private _spawnLocation = getMarkerPos (if(player call isAttacker) then {"AttackerSpawn"} else {"DefenderSpawn"});
private _oldPos = getPosASL player;
player setVariable ["spawning",true,true];
player setPosASL _spawnLocation;
waitUntil{ not(_oldPos isEqualTo (getPosASL player))};
player setVariable ["spawning",false,true];
1 call NikkoClient_script_destroyLoadingScreen;

//Interval
waitUntil{uiSleep 0.9; hint format ["New Round Interval: %1 Till Next Round", (floor(_roundInterval - serverTime) call getRoundTimeLeft)]; serverTime > _roundInterval};
 
//New Round
hint format ["New Round Started: %1\n Till Round Over", _maxRoundTime call getRoundTimeLeft];

//Spawn player
[Nikko_Var_selectedMissionLocation] spawn NikkoClient_script_missionSpawn;

//Round Ended & Death handler
[] spawn {
	waitUntil{!alive player || !(missionNamespace getVariable ["Nikko_Var_RoundInProgress", true])};
	if(!alive player)then{
		[true] spawn NikkoClient_script_WaitingRoundEnd;//dead
	}else{
		[false] spawn NikkoClient_script_WaitingRoundEnd;//rnd end
	};
};