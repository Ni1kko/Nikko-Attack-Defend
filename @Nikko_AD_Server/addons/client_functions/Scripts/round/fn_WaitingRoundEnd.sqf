/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/
params [ 
	["_isDead", false, [false]]
]; 

private _isAttacker = (player call isAttacker);
private _players = (if(_isAttacker)then {Nikko_Var_Attackers}else{Nikko_Var_Defenders});


//Wait till round is over 
private _verdict = "";
private _RndEndMessages = [
	"Capture and Defend: a mission where one team will try capture whilst the over team battle to defend the flag",
	"Attack and Defend: framework made by Ni1kko"
];
waitUntil{
	uiSleep random [3,5,10];

	if(_isDead)then{ 
		private _msg = selectRandom _RndEndMessages; 
		if(count Nikko_Var_waitingRndEndMessageQueue > 0)then{ 
			private _i = random ((count Nikko_Var_waitingRndEndMessageQueue) - 1); 
			_msg = Nikko_Var_waitingRndEndMessageQueue#_i; 
			Nikko_Var_waitingRndEndMessageQueue deleteAt _i;
		};
		['Waiting Too Be Redeployed',_msg] call NikkoClient_script_updateLoadingScreen; 
	};

	_verdict = (call getRoundVedict); 
	_verdict != ""
};

private _title = format ["Round (%1) Over", Nikko_Var_RoundsCount];
_verdict = switch (_verdict) do {
	case "Defenders":  { (if(!_isAttacker)then{"Congrats | "}else{"Better Luck Next Time | "}) + "Attackers Won" };
	case "Attackers":  { (if( _isAttacker)then{"Congrats | "}else{"Better Luck Next Time | "}) + "Defenders Won" };
	case "Time": 	   { "Time Reached" };
	case "Captured":   { (if( _isAttacker)then{"Congrats | "}else{"Better Luck Next Time | "}) + "Flag Captured" };
};

[_title,_verdict] call NikkoClient_script_updateLoadingScreen;
uiSleep 4;
 