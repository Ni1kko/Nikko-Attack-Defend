/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/
params [ 
	["_roundInterval", 0, [0]],
	["_maxRoundTime", 0, [0]]
]; 

Hint format ["Round Ended | Next Round Starts in %1 Seconds", _roundInterval];
 
//back to spawn island
private _oldPos = getPosASL player;
player setVariable ["spawning",true,true];
markerPos ["respawn_guerrila",true];
_markerPos = (markerPos ["respawn_guerrila",true]);
_markerPos set [2, 3];
player setPosASL _markerPos;
[player] joinSilent (missionNamespace getVariable ["spawnGroup", grpNull]);
waitUntil{ not(_oldPos isEqualTo (getPosASL player))};
player setVariable ["spawning",false,true];
player setDamage 0;
