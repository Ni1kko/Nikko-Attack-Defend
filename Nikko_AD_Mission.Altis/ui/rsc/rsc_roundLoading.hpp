/*
	Nikko Renolds
	Attack & Defend
	Ni1kko@outlook.com
*/

class Nikko_Rsc_LoadingScreen {
	idd = 70000; 
    onLoad = "uiNamespace setVariable ['Nikko_Rsc_LoadingScreen',_this select 0]";
	fadein = 0;
	duration = 1e+011;
	fadeout = 0;

	class Controls { 
		class FullscreenBackground: RscText {
			idc = -1;
			x = "safezoneXAbs";
			y = "safezoneY";
			w = "safezoneWAbs";
			h = "safezoneH";
			colorBackground[] = { 0, 0, 0, 1 };
		}; 
		class SplashNoise: RscPicture {
			idc = -1;
			x = "safezoneXAbs";
			y = "safezoneY";
			w = "safezoneWAbs";
			h = "safezoneH";
			text = "\A3\Ui_f\data\IGUI\RscTitles\SplashArma3\arma3_splashNoise_ca.paa"; 
		}; 
        class MessageQueue : RscStructuredText
        {
            idc = 100; 
            x = 0;
            y = 0.22;
            w = 1;
            h = 0.75 * safezoneH;  
 
            text = ""; 
            shadow = 1;

            headerTextSize = 2.9; 
            bodyTextSize = 1.5;

            headerTextColor = "#d2262b"; //Red
            bodyTextColor = "#ebebeb";   //White
 
            class Attributes { 
				align = "center";
				valign = "top"; 
			};
        };  
	}; 
};
 